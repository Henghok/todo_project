import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
// import Login from "../views/Login.vue";
import SignIn from "../views/signin.vue";
import Register from "../views/signup";
import Profile from "../views/Profile.vue";
import Task from "../views/TaskDetail.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  { path: "/login", name: "login", component: SignIn },
  { path: "/register", name: "reg", component: Register },
  {
    path: "/about",
    name: "About",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/About.vue"),
  },
  {
    path: "/profile",
    component: Profile,
  },
  {
    path: "/task/:id",
    component: Task,
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

router.beforeEach((to, from, next) => {
  if (
    to.name !== "login" &&
    !localStorage.getItem("token") &&
    to.name !== "reg"
  )
    next({ name: "login" });
  else if (to.name === "login" && localStorage.getItem("token"))
    next({ name: "Home" });
  else next();
});

export default router;
