<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class userProject2 extends Model
{
    use HasFactory;

    protected $table = 'user_projects';

    protected $fillable = ['user_id', 'project_id'];

    public function project()
    {
        return $this->belongsToMany(Project::class);
    }
}
