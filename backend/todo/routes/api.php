<?php

use App\Http\Controllers\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\TodoController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => ['auth:sanctum']], function () {
    Route::get('/test', function (Request $request) {
        echo $request->user();
        return response('Hello World', 200)
            ->header('Content-Type', 'text/plain');
    });
    Route::post('/logout', [AuthController::class, 'logout']);

    Route::resource('/cate', 'CategoryController');

    Route::resource('/todo', 'TodoController');

    Route::get('/todo/{id}', [TodoController::class, 'show']);

    Route::resource('/check', 'CheckListController');

    Route::resource('/project', 'projectController');
});
Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/login', [AuthController::class, 'login']);

Route::post('/rg', [AuthController::class, 'register']);
